using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ImageQuantization
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        RGBPixel[,] ImageMatrix;

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Open the browsed image and display it
                string OpenedFilePath = openFileDialog1.FileName;
                ImageMatrix = ImageOperations.OpenImage(OpenedFilePath);
                ImageOperations.DisplayImage(ImageMatrix, pictureBox1);
            }            
            
            txtWidth.Text = ImageOperations.GetWidth(ImageMatrix).ToString();
            txtHeight.Text = ImageOperations.GetHeight(ImageMatrix).ToString();
        }

        private void btnGaussSmooth_Click(object sender, EventArgs e)
        {

            List<RGBPixel> Distinct = ImageOperations.FindDistinctColors(ImageMatrix);
            ImageOperations.FillIdToColor9(Distinct);
            List<edge> minimumspanningtreeGraph = ImageOperations.Mst(Distinct);
            int numberofCluster = Convert.ToInt32(cluster.Text);
            List<edge> ClusterDivideter = ImageOperations.DividingClusters(minimumspanningtreeGraph, numberofCluster);
            Dictionary<int, List<int>> adjacentListOfEdges = ImageOperations.AdjacencyMatrixOfMST(ClusterDivideter);
            ImageOperations.ClusterRepresentativeColor(adjacentListOfEdges);
            ImageOperations.UpdateTheImage(ImageMatrix, numberofCluster);
            double sigma = double.Parse(txtGaussSigma.Text);
            int maskSize = (int)nudMaskSize.Value;
            ImageMatrix = ImageOperations.GaussianFilter1D(ImageMatrix, maskSize, sigma);
            ImageOperations.DisplayImage(ImageMatrix, pictureBox2);

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

       
       
    }
}