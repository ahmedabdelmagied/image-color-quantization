using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System.Drawing.Imaging;

namespace ImageQuantization
{
    /// <summary>
    /// Holds the pixel color in 3 byte values: red, green and blue
    /// </summary>
    public struct RGBPixel
    {
        public byte red, green, blue;
        public int id;
    }
    public struct RGBPixelD
    {
        public double red, green, blue;
    }
    public struct edge : IComparable<edge>
    {
        public int id1 { get; set; }
        public int id2 { get; set; }
        public double cost { get; set; }
        public int CompareTo(edge other)
        {
            if (this.cost > other.cost) return 1;
            else if (this.cost < other.cost) return -1;
            return 0;
        }
        public edge(int id1, int id2, double cost)
            : this()
        {
            this.id1 = id1;
            this.id2 = id2;
            this.cost = cost;
        }
    }
 
  
    /// <summary>
    /// Library of static functions that deal with images
    /// </summary>
    public class ImageOperations
    {

        /// <summary>
        /// Open an image and load it into 2D array of colors (size: Height x Width)
        /// </summary>
        /// <param name="ImagePath">Image file path</param>
        /// <returns>2D array of colors</returns>
        public static RGBPixel[,] OpenImage(string ImagePath)
        {
            Bitmap original_bm = new Bitmap(ImagePath);
            int Height = original_bm.Height;
            int Width = original_bm.Width;

            RGBPixel[,] Buffer = new RGBPixel[Height, Width];

            unsafe
            {
                BitmapData bmd = original_bm.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.ReadWrite, original_bm.PixelFormat);
                int x, y;
                int nWidth = 0;
                bool Format32 = false;
                bool Format24 = false;
                bool Format8 = false;

                if (original_bm.PixelFormat == PixelFormat.Format24bppRgb)
                {
                    Format24 = true;
                    nWidth = Width * 3;
                }
                else if (original_bm.PixelFormat == PixelFormat.Format32bppArgb || original_bm.PixelFormat == PixelFormat.Format32bppRgb || original_bm.PixelFormat == PixelFormat.Format32bppPArgb)
                {
                    Format32 = true;
                    nWidth = Width * 4;
                }
                else if (original_bm.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    Format8 = true;
                    nWidth = Width;
                }
                int nOffset = bmd.Stride - nWidth;
                byte* p = (byte*)bmd.Scan0;
                for (y = 0; y < Height; y++)
                {
                    for (x = 0; x < Width; x++)
                    {
                        if (Format8)
                        {
                            Buffer[y, x].red = Buffer[y, x].green = Buffer[y, x].blue = p[0];
                            p++;
                        }
                        else
                        {
                            Buffer[y, x].red = p[2];
                            Buffer[y, x].green = p[1];
                            Buffer[y, x].blue = p[0];
                            if (Format24) p += 3;
                            else if (Format32) p += 4;
                        }
                    }
                    p += nOffset;
                }
                original_bm.UnlockBits(bmd);
            }

            return Buffer;
        }

        static int[, ,] Check = new int[256, 256, 256];
        public static RGBPixel[] ColorOfId;
        static int[, ,] RepresentativeColor = new int[256, 256, 256];
        public static RGBPixelD[] ColorId1;
        public static int Cid = 1;

        /// <summary>
        /// A Function That Calculate The Distinct Color Of The 2D image ..
        /// </summary>
        /// <param name="ImageMatrix"></param>
        /// <returns>Returns a List Contains The Distinct Color of The ImageMatrix..</returns>
        public static List<RGBPixel> FindDistinctColors(RGBPixel[,] ImageMatrix)
        {

            List<RGBPixel> DistinctColors = new List<RGBPixel>();
            int height = GetHeight(ImageMatrix);
            int width = GetWidth(ImageMatrix);
            int id = 1; 
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    RGBPixel CurrentColor = ImageMatrix[i, j];

                    if (Check[CurrentColor.red, CurrentColor.green, CurrentColor.blue] == 0){
                        CurrentColor.id = id++;
                        Check[CurrentColor.red, CurrentColor.green, CurrentColor.blue] = 1;
                        DistinctColors.Add(CurrentColor);

                    }
                }
            }

            return DistinctColors;
        }

        /// <summary>
        ///   Function Calculate Edges Of The Minimum Spanning Tree Using The Minimum Heap DataStructure ..
        /// </summary>
        /// <param name="DistinceColor"></param>
        /// <returns>A List Contains The Edges Of The Minimum Spanning Tree..</returns>
        public static List<edge> Mst(List<RGBPixel> DistinceColor)  
        {
            double TotalCostMst = 0; 
            List<edge> result = new List<edge>();
            double EclideanCost = 0;
            List<double> Edgescost = Enumerable.Repeat(double.MaxValue, DistinceColor.Count).ToList();
            List<int> parent= Enumerable.Repeat(-1, DistinceColor.Count).ToList();
            Edgescost[0] = 0;
            int nextnode = 0;
            int[] visitedMst = new int[DistinceColor.Count];
        
            int distinctColorSize = DistinceColor.Count;
            for (int i = 0; i < distinctColorSize; i++) 
            {
                int NodeIndex = nextnode;
                visitedMst[NodeIndex] = 1;
                double mininode = double.MaxValue;

                for (int j = 0; j < distinctColorSize; j++) 
                {

                    if (visitedMst[j]==0) 
                    {
                        EclideanCost = CalculateEclidean(DistinceColor[NodeIndex], DistinceColor[j]);
                        if (EclideanCost < Edgescost[j]){
                            parent[j] = NodeIndex;
                            Edgescost[j] = EclideanCost;
                        }
                        if (Edgescost[j] < mininode){
                            mininode = Edgescost[j];
                            nextnode = j;
                        }

                    }
                }
            }

            TotalCostMst = 0;
            int parentSize = parent.Count;
            for (int i = 1; i < parentSize; i++)
            {
                edge newedge = new edge();
                     newedge.id1 = parent[i]+1;
                     newedge.id2 = i+1;
                     newedge.cost = Edgescost[i];

                result.Add(newedge);
                TotalCostMst += Edgescost[i];
            }
            MessageBox.Show(TotalCostMst.ToString());
            return result;  
        }

        /// <summary>
        /// A Simple Function That Make Adjacency List between Dividing Custers edges
        /// </summary>
        /// <param name="dividingClusters">A List Of Dividing Clusters Edges..</param>
        ///
        public static Dictionary<int, List<int>> AdjacencyMatrixOfMST(List<edge> dividingClusters) 
        {
            Dictionary<int, List<int>> adjacentListOfEdges;
            adjacentListOfEdges = new Dictionary<int, List<int>>();
            int ClusterSize = dividingClusters.Count;

            for (int i = 0; i < ClusterSize; i++)   
            {
                edge NextEdge = dividingClusters[i];     
                List<int> e = new List<int>();
                List<int> ee = new List<int>();

                if (adjacentListOfEdges.ContainsKey(NextEdge.id1)){
                    e = adjacentListOfEdges[NextEdge.id1];
                    e.Add(NextEdge.id2);  
                    adjacentListOfEdges[NextEdge.id1] = e;  
                }
                else{
                    e.Add(NextEdge.id2);
                    adjacentListOfEdges[NextEdge.id1] = e;
                }

                if (adjacentListOfEdges.ContainsKey(NextEdge.id2)){
                    ee = adjacentListOfEdges[NextEdge.id2];
                    ee.Add(NextEdge.id1); 
                    adjacentListOfEdges[NextEdge.id2] = ee;
                }
                else{
                    ee.Add(NextEdge.id1);
                    adjacentListOfEdges[NextEdge.id2] = ee;
                }       
      
            }
            return adjacentListOfEdges;  
        }
        

        public static List<int> BeginningClusters = new List<int>();
        public static int[] BeginningClusterVisited = new int[9000000];
        /// <summary>
        ///    Functions That Divide The Minimum Spanning Tree into Given K Clusters
        /// </summary>
        /// <param name="Mst"></param>
        /// <param name="k">The number of needed clusters</param>
        /// <returns></returns>
       
        public static List<edge> DividingClusters(List<edge> Mst, int k)
        {
            for (int i = 0; i < k - 1 ; i++)  {
                edge removedEdge = new edge();
                double MaxCost = -1;
                int RemovedIndex = -1;  
                int SizeOfMsT = Mst.Count; 
                for (int j = 0; j < SizeOfMsT; j++){
                    edge Edge = Mst[j];
                    if (Edge.cost > MaxCost)
                    {
                        RemovedIndex = j;
                        MaxCost = Edge.cost;
                        removedEdge = Mst[j];
                    }
                    
                }
                if (BeginningClusterVisited[removedEdge.id1] == 0){
                    BeginningClusterVisited[removedEdge.id1] = 1;
                    BeginningClusters.Add(removedEdge.id1);
                }
                 if (BeginningClusterVisited[removedEdge.id2] == 0){
                    BeginningClusterVisited[removedEdge.id2] = 1;
                    BeginningClusters.Add(removedEdge.id2);
                }
                Mst.RemoveAt(RemovedIndex);
            }
          
            return Mst;
        }



        static int[, ,] IdToRepresentativeColor = new int[256, 256, 256];
        public static List<RGBPixel> RepresentativeColorOfEachCluster = new List<RGBPixel>();
        public static int IdCounter = 0;
        public static int[] Visisted = new int[90000];
        public static void BFS(int BeginningCluster, Dictionary<int, List<int>> adjacentListOfEdges)
        {
            RGBPixelD RepresentativeC = new RGBPixelD();
            int ClusterCounter = 0;
            Queue<int> q = new Queue<int>();
            q.Enqueue(BeginningCluster);
            while (q.Count !=0) 
            {
                int CurrentNode = q.Dequeue();
                RepresentativeC.red +=  IdToColor[CurrentNode].red;
                RepresentativeC.green += IdToColor[CurrentNode].green;
                RepresentativeC.blue += IdToColor[CurrentNode].blue;

                Visisted[CurrentNode] = 1; 

                if (adjacentListOfEdges.ContainsKey(CurrentNode))
                {
                    List<int> ChildNodes = adjacentListOfEdges[CurrentNode];
                    int ChildNodesSize = ChildNodes.Count;
                    for (int i = 0; i < ChildNodesSize; i++)  
                    {
                        if(Visisted[ChildNodes[i]] == 0)
                           q.Enqueue(ChildNodes[i]);
                    }
                }
               
                ClusterCounter++; 
                IdToRepresentativeColor[IdToColor[CurrentNode].red, IdToColor[CurrentNode].green, IdToColor[CurrentNode].blue] = IdCounter;
            }
            
            RepresentativeC.red /= ClusterCounter;
            RepresentativeC.green /=ClusterCounter;
            RepresentativeC.blue /= ClusterCounter;

            RGBPixel Representative = new RGBPixel();
            Representative.red = (byte)RepresentativeC.red;
            Representative.green = (byte)RepresentativeC.green;
            Representative.blue = (byte)RepresentativeC.blue;

            RepresentativeColorOfEachCluster.Add(Representative);
        }

        public static void ClusterRepresentativeColor(Dictionary<int, List<int>> adjacentListOfEdges) 
        {
            int size = BeginningClusters.Count; 
            for (int i = 0; i < size; i++) 
            {
                BFS(BeginningClusters[i], adjacentListOfEdges);
                IdCounter++;
            }
        }

        public static void UpdateTheImage(RGBPixel[,] ImageMatrix ,int k )
        {
            int height = GetHeight(ImageMatrix);
            int width = GetWidth(ImageMatrix);
            
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    RGBPixel Current = ImageMatrix[i, j];
                    if(k!=0)
                    ImageMatrix[i, j] = RepresentativeColorOfEachCluster[IdToRepresentativeColor[Current.red, Current.green, Current.blue]];
                }
            }
        }

        /// <summary>
        /// a simple function to calculate the eclidean distance between 2 colors...
        /// </summary>
        /// <param name="FirstColor"></param>
        /// <param name="SecondColor"></param>
        /// <returns></returns>
        public static double CalculateEclidean(RGBPixel FirstColor, RGBPixel SecondColor)
        {
            int RedDifferenceColor = FirstColor.red - SecondColor.red;

            int GreenDifferenceColor = FirstColor.green - SecondColor.green;

            int BlueDifferenceColor = FirstColor.blue - SecondColor.blue;

            return Math.Sqrt((RedDifferenceColor * RedDifferenceColor) + (GreenDifferenceColor * GreenDifferenceColor) + (BlueDifferenceColor * BlueDifferenceColor));
        }

        public static RGBPixel[] IdToColor; // index starts from 1..
        /// <summary>
        /// For each id it put it's corresponding color in the array Called IdToColor...
        /// </summary>
        /// <param name="Distinct"></param>
        public static void FillIdToColor9(List<RGBPixel> Distinct)
        {
            IdToColor = new RGBPixel[Distinct.Count + 1];
            for (int i = 0; i < Distinct.Count; i++)
            {
                IdToColor[Distinct[i].id] = Distinct[i];
            }
        }
 
        /// <summary>
        /// Get the height of the image 
        /// </summary>
        /// <param name="ImageMatrix">2D array that contains the image</param>
        /// <returns>Image Height</returns>
        public static int GetHeight(RGBPixel[,] ImageMatrix)
        {
          if(ImageMatrix != null)
            return ImageMatrix.GetLength(0);
          return 0;
        }


        /// <summary>
        // Get the width of the image 
        /// </summary>
        /// <param name="ImageMatrix">2D array that contains the image</param>
        /// <returns>Image Width</returns>
        public static int GetWidth(RGBPixel[,] ImageMatrix)
        {
            if(ImageMatrix != null)
                return ImageMatrix.GetLength(1);
            return 0;
          
        }

        /// <summary>
        /// Display the given image on the given PictureBox object
        /// </summary>
        /// <param name="ImageMatrix">2D array that contains the image</param>
        /// <param name="PicBox">PictureBox object to display the image on it</param>
        public static void DisplayImage(RGBPixel[,] ImageMatrix, PictureBox PicBox)
        {
            // Create Image:
            //==============
            int Height = ImageMatrix.GetLength(0);
            int Width = ImageMatrix.GetLength(1);

            Bitmap ImageBMP = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);

            unsafe
            {
                BitmapData bmd = ImageBMP.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.ReadWrite, ImageBMP.PixelFormat);
                int nWidth = 0;
                nWidth = Width * 3;
                int nOffset = bmd.Stride - nWidth;
                byte* p = (byte*)bmd.Scan0;
                for (int i = 0; i < Height; i++)
                {
                    for (int j = 0; j < Width; j++)
                    {
                        p[2] = ImageMatrix[i, j].red;
                        p[1] = ImageMatrix[i, j].green;
                        p[0] = ImageMatrix[i, j].blue;
                        p += 3;
                    }

                    p += nOffset;
                }
                ImageBMP.UnlockBits(bmd);
            }
            PicBox.Image = ImageBMP;
        }


       /// <summary>
       /// Apply Gaussian smoothing filter to enhance the edge detection 
       /// </summary>
       /// <param name="ImageMatrix">Colored image matrix</param>
       /// <param name="filterSize">Gaussian mask size</param>
       /// <param name="sigma">Gaussian sigma</param>
       /// <returns>smoothed color image</returns>
        public static RGBPixel[,] GaussianFilter1D(RGBPixel[,] ImageMatrix, int filterSize, double sigma)
        {
            int Height = GetHeight(ImageMatrix);
            int Width = GetWidth(ImageMatrix);

            RGBPixelD[,] VerFiltered = new RGBPixelD[Height, Width];
            RGBPixel[,] Filtered = new RGBPixel[Height, Width];

           
            // Create Filter in Spatial Domain:
            //=================================
            //make the filter ODD size
            if (filterSize % 2 == 0) filterSize++;

            double[] Filter = new double[filterSize];

            //Compute Filter in Spatial Domain :
            //==================================
            double Sum1 = 0;
            int HalfSize = filterSize / 2;
            for (int y = -HalfSize; y <= HalfSize; y++)
            {
                //Filter[y+HalfSize] = (1.0 / (Math.Sqrt(2 * 22.0/7.0) * Segma)) * Math.Exp(-(double)(y*y) / (double)(2 * Segma * Segma)) ;
                Filter[y + HalfSize] = Math.Exp(-(double)(y * y) / (double)(2 * sigma * sigma));
                Sum1 += Filter[y + HalfSize];
            }
            for (int y = -HalfSize; y <= HalfSize; y++)
            {
                Filter[y + HalfSize] /= Sum1;
            }

            //Filter Original Image Vertically:
            //=================================
            int ii, jj;
            RGBPixelD Sum;
            RGBPixel Item1;
            RGBPixelD Item2;

            for (int j = 0; j < Width; j++)
                for (int i = 0; i < Height; i++)
                {
                    Sum.red = 0;
                    Sum.green = 0;
                    Sum.blue = 0;
                    for (int y = -HalfSize; y <= HalfSize; y++)
                    {
                        ii = i + y;
                        if (ii >= 0 && ii < Height)
                        {
                            Item1 = ImageMatrix[ii, j];
                            Sum.red += Filter[y + HalfSize] * Item1.red;
                            Sum.green += Filter[y + HalfSize] * Item1.green;
                            Sum.blue += Filter[y + HalfSize] * Item1.blue;
                        }
                    }
                    VerFiltered[i, j] = Sum;
                }

            //Filter Resulting Image Horizontally:
            //===================================
            for (int i = 0; i < Height; i++)
                for (int j = 0; j < Width; j++)
                {
                    Sum.red = 0;
                    Sum.green = 0;
                    Sum.blue = 0;
                    for (int x = -HalfSize; x <= HalfSize; x++)
                    {
                        jj = j + x;
                        if (jj >= 0 && jj < Width)
                        {
                            Item2 = VerFiltered[i, jj];
                            Sum.red += Filter[x + HalfSize] * Item2.red;
                            Sum.green += Filter[x + HalfSize] * Item2.green;
                            Sum.blue += Filter[x + HalfSize] * Item2.blue;
                        }
                    }
                    Filtered[i, j].red = (byte)Sum.red;
                    Filtered[i, j].green = (byte)Sum.green;
                    Filtered[i, j].blue = (byte)Sum.blue;
                }

            return Filtered;
        }


    }
}
