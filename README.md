# Image-Color-Quantization
The idea of the project is to reduce the number of colors in a full resolution digital color (24 bit per pixel) to                            a  smaller set of representative color  where  image differ as little as possible from original image . 

# ScreenShots

<p align="center">
<img src="https://user-images.githubusercontent.com/17562667/42283743-b4133eae-7faa-11e8-9854-1976db47e2e0.jpg" width="590" height="420">
  
  <img src="https://user-images.githubusercontent.com/17562667/42283753-b8296ad6-7faa-11e8-811b-7b176169dad1.jpg" width="590" height="420">

  
  <img src="https://user-images.githubusercontent.com/17562667/42283771-c2183c66-7faa-11e8-930a-a8eb2b871c8a.jpg" width="590" height="420">
<p>
